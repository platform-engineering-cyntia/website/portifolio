---
title: "Aprenda java"
---

# Aprenda java

Este site é dedicado ao ensino gratuído da linguagem java, assim como alguns conceitos da plataforma java.

Este site faz parte do projeto Economia na Informática que busca trazer uma forma mais economica para trabalhar com Tecnologia da Informação.


## Principais assuntos

* [O que é java]
* [Entendendo o SDK]
* [Ferramentas para iniciar em java]
* [A primeira implementação]

![Screenshot](https://github.com/matcornic/hugo-theme-learn/raw/master/images/screenshot.png?width=40pc&classes=shadow)
